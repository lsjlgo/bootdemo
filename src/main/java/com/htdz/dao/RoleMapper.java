package com.htdz.dao;

import com.htdz.entity.Resource;
import com.htdz.entity.Role;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* Created by gaea-autocoding on 2018-03-20 15:09:07
*/

@Repository
public interface RoleMapper {

    List<Role> listBy(Map map);

    void insert(Map map);

    void delete(Long roleId);

}
