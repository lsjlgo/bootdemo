package com.htdz.dao;

import com.htdz.entity.Resource;
import com.htdz.entity.Users;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* Created by gaea-autocoding on 2018-03-20 15:09:07
*/

@Repository
public interface ResourceMapper {

    List<Resource> listBy(Map map);

    Resource getById(Long resourceId);

    Resource getParentResourceByResourceId(Long resourceId);

}
