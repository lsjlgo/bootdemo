package com.htdz.dao;

import cn.jarod.myframework.gaeacore.base.dao.BaseDao;
import com.htdz.entity.Users;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* Created by gaea-autocoding on 2018-03-20 15:09:07
*/

@Repository
public interface UsersDao extends BaseDao<Users> {


    List<Users> selectAll(Map map);
}
