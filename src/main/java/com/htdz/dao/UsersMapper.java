package com.htdz.dao;

import com.htdz.entity.Role;
import com.htdz.entity.Users;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* Created by gaea-autocoding on 2018-03-20 15:09:07
*/

@Repository
public interface UsersMapper {


    List<Users> selectAll();

    List<Users> listBy(Map map);

    void delete(Long usersId);

    void insert(Map map);

}
