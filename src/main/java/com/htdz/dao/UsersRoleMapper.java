package com.htdz.dao;

import com.htdz.entity.RoleResource;
import com.htdz.entity.UsersRole;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* Created by gaea-autocoding on 2018-03-20 15:09:07
*/

@Repository
public interface UsersRoleMapper {

    List<UsersRole> listBy(Map map);

    void insert(Map map);

    void delete(Long usersId);

    void deleteByRoleId(Long roleId);

}
