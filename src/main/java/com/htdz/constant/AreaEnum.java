package com.htdz.constant;

public enum AreaEnum {
    //浙江省
    HANG_ZHOU("05-0571","杭州市"),
    HU_ZHOU("05-0572","湖州市"),
    JIA_XIN("05-0573","嘉兴市"),
    NING_BO("05-0574","宁波市"),
    SHAO_XIN("05-0575","绍兴市"),
    TAI_ZHOU("05-0576","台州市"),
    WEI_ZHOU("05-0577","温州市"),
    LIS_SHUI("05-0578","丽水市"),
    JIN_HUA("05-0579","金华市"),
    ZHOU_SHAN("05-0580","舟山市"),
    //广东省
    GUANG_ZHOU("07-020","广州市"),
    SHAO_GUAN("07-0751","韶关市"),
    HUI_ZHOU("07-0752","惠州市 "),
    MEI_ZHOU("07-0753","梅州市"),
    SHAN_TOU("07-0754","汕头市"),
    SHENG_ZHEN("07-0755","深圳市"),
    ZHU_HAI("07-0756","珠海市"),
    FOU_SHAN("07-0757","佛山市");

    public String code;
    public String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
    AreaEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public static String getCodeByValue(String value) {
        if (value != null) {
            for (AreaEnum areaEnum : AreaEnum.values()) {
                if (areaEnum.value.equals(value)) {
                    return areaEnum.getCode();
                }
            }
        }
        return null;
    }
}
