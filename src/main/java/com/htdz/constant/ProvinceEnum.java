package com.htdz.constant;

public enum ProvinceEnum {
    ZHE_JIANG("05","浙江省"),
    GUANG_ZHOU("07","广东省");

    private String code;
    private String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
    ProvinceEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public static String getCodeByValue(String value) {
        if (value != null) {
            for (ProvinceEnum areaEnum : ProvinceEnum.values()) {
                if (areaEnum.value.equals(value)) {
                    return areaEnum.getCode();
                }
            }
        }
        return null;
    }
}
