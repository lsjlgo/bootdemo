package com.htdz.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

//job内容
@Component
public class DemoJob implements SimpleJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void execute(ShardingContext shardingContext) {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info(" ==> "+Thread.currentThread().getId());
    }
}
