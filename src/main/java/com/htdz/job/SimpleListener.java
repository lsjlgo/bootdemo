package com.htdz.job;

import com.dangdang.ddframe.job.executor.ShardingContexts;
import com.dangdang.ddframe.job.lite.api.listener.ElasticJobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.Calendar;

//监听器，用于job执行前后，日志

public class SimpleListener implements ElasticJobListener {
    private static Logger logger = LoggerFactory.getLogger(SimpleListener.class);
    private long startDateMills ;
    private long endDateMills;


    public void beforeJobExecuted(final ShardingContexts shardingContexts) {
        startDateMills = Calendar.getInstance().getTimeInMillis();
        logger.info("beforeJobExecuted:" + shardingContexts.getJobName());
    }

    public void afterJobExecuted(final ShardingContexts shardingContexts) {
        endDateMills = Calendar.getInstance().getTimeInMillis();
        DecimalFormat df = new DecimalFormat("0.00");
        logger.info("afterJobExecuted:" + shardingContexts.getJobName()+" "+df.format((float)(endDateMills-startDateMills)/1000)+" s");
    }
}