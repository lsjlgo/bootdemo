
package com.htdz.webservice.cxf;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>DT_SaleArea2CityDist_Res complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="DT_SaleArea2CityDist_Res"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ITEMS" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BZIRK" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BZTXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ZPID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ZPDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ZCID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ZCDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ZDID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ZDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="VKORG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="VTEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ERDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_SaleArea2CityDist_Res", propOrder = {
    "items"
})
public class DTSaleArea2CityDistRes {

    @XmlElement(name = "ITEMS")
    protected List<ITEMS> items;

    /**
     * Gets the value of the items property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the items property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getITEMS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ITEMS }
     * 
     * 
     */
    public List<ITEMS> getITEMS() {
        if (items == null) {
            items = new ArrayList<ITEMS>();
        }
        return this.items;
    }


    /**
     * <p>anonymous complex type�� Java �ࡣ
     * 
     * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BZIRK" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BZTXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ZPID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ZPDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ZCID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ZCDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ZDID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ZDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="VKORG" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="VTEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ERDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pid",
        "bzirk",
        "bztxt",
        "zpid",
        "zpdesc",
        "zcid",
        "zcdesc",
        "zdid",
        "zddesc",
        "vkorg",
        "vtext",
        "erdate"
    })
    public static class ITEMS {

        @XmlElement(name = "PID", required = true)
        protected String pid;
        @XmlElement(name = "BZIRK", required = true)
        protected String bzirk;
        @XmlElement(name = "BZTXT")
        protected String bztxt;
        @XmlElement(name = "ZPID", required = true)
        protected String zpid;
        @XmlElement(name = "ZPDESC")
        protected String zpdesc;
        @XmlElement(name = "ZCID")
        protected String zcid;
        @XmlElement(name = "ZCDESC")
        protected String zcdesc;
        @XmlElement(name = "ZDID")
        protected String zdid;
        @XmlElement(name = "ZDDESC")
        protected String zddesc;
        @XmlElement(name = "VKORG", required = true)
        protected String vkorg;
        @XmlElement(name = "VTEXT")
        protected String vtext;
        @XmlElement(name = "ERDATE")
        protected String erdate;

        /**
         * ��ȡpid���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPID() {
            return pid;
        }

        /**
         * ����pid���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPID(String value) {
            this.pid = value;
        }

        /**
         * ��ȡbzirk���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBZIRK() {
            return bzirk;
        }

        /**
         * ����bzirk���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBZIRK(String value) {
            this.bzirk = value;
        }

        /**
         * ��ȡbztxt���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBZTXT() {
            return bztxt;
        }

        /**
         * ����bztxt���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBZTXT(String value) {
            this.bztxt = value;
        }

        /**
         * ��ȡzpid���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZPID() {
            return zpid;
        }

        /**
         * ����zpid���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZPID(String value) {
            this.zpid = value;
        }

        /**
         * ��ȡzpdesc���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZPDESC() {
            return zpdesc;
        }

        /**
         * ����zpdesc���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZPDESC(String value) {
            this.zpdesc = value;
        }

        /**
         * ��ȡzcid���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZCID() {
            return zcid;
        }

        /**
         * ����zcid���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZCID(String value) {
            this.zcid = value;
        }

        /**
         * ��ȡzcdesc���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZCDESC() {
            return zcdesc;
        }

        /**
         * ����zcdesc���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZCDESC(String value) {
            this.zcdesc = value;
        }

        /**
         * ��ȡzdid���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZDID() {
            return zdid;
        }

        /**
         * ����zdid���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZDID(String value) {
            this.zdid = value;
        }

        /**
         * ��ȡzddesc���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZDDESC() {
            return zddesc;
        }

        /**
         * ����zddesc���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZDDESC(String value) {
            this.zddesc = value;
        }

        /**
         * ��ȡvkorg���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVKORG() {
            return vkorg;
        }

        /**
         * ����vkorg���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVKORG(String value) {
            this.vkorg = value;
        }

        /**
         * ��ȡvtext���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVTEXT() {
            return vtext;
        }

        /**
         * ����vtext���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVTEXT(String value) {
            this.vtext = value;
        }

        /**
         * ��ȡerdate���Ե�ֵ��
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getERDATE() {
            return erdate;
        }

        /**
         * ����erdate���Ե�ֵ��
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setERDATE(String value) {
            this.erdate = value;
        }

    }

}
