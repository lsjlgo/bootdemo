
package com.htdz.webservice.cxf;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.transfar.salearea2citydist package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTSaleArea2CityDistRes_QNAME = new QName("http://transfar.com/SaleArea2CityDist", "MT_SaleArea2CityDist_Res");
    private final static QName _MTSaleArea2CityDistReq_QNAME = new QName("http://transfar.com/SaleArea2CityDist", "MT_SaleArea2CityDist_Req");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.transfar.salearea2citydist
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTSaleArea2CityDistRes }
     * 
     */
    public DTSaleArea2CityDistRes createDTSaleArea2CityDistRes() {
        return new DTSaleArea2CityDistRes();
    }

    /**
     * Create an instance of {@link DTSaleArea2CityDistReq }
     * 
     */
    public DTSaleArea2CityDistReq createDTSaleArea2CityDistReq() {
        return new DTSaleArea2CityDistReq();
    }

    /**
     * Create an instance of {@link DTSaleArea2CityDistRes.ITEMS }
     * 
     */
    public DTSaleArea2CityDistRes.ITEMS createDTSaleArea2CityDistResITEMS() {
        return new DTSaleArea2CityDistRes.ITEMS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTSaleArea2CityDistRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://transfar.com/SaleArea2CityDist", name = "MT_SaleArea2CityDist_Res")
    public JAXBElement<DTSaleArea2CityDistRes> createMTSaleArea2CityDistRes(DTSaleArea2CityDistRes value) {
        return new JAXBElement<DTSaleArea2CityDistRes>(_MTSaleArea2CityDistRes_QNAME, DTSaleArea2CityDistRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTSaleArea2CityDistReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://transfar.com/SaleArea2CityDist", name = "MT_SaleArea2CityDist_Req")
    public JAXBElement<DTSaleArea2CityDistReq> createMTSaleArea2CityDistReq(DTSaleArea2CityDistReq value) {
        return new JAXBElement<DTSaleArea2CityDistReq>(_MTSaleArea2CityDistReq_QNAME, DTSaleArea2CityDistReq.class, null, value);
    }

}
