package com.htdz.service;

import com.htdz.entity.Node;
import com.htdz.entity.Resource;
import com.htdz.entity.Users;
import com.htdz.entity.UsersRole;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface ResourceService {

	List<Resource> listResource(Resource resource, Integer pageNum);

	List<Node> listResourceTree(Resource resource,Long roleId,Boolean isLeftMenu);

	List<Node> listMenuResourceTree(Resource resource, List<UsersRole> usersRoleList);

	@Transactional
	void saveRoleResource(Long roleId,String checkedIds);

	Map listMenuResource(List<UsersRole> usersRoleList);

	Resource getParentResourceByResourceId(Long resourceId);
}
