package com.htdz.service;

import java.util.List;
import java.util.Map;

import com.htdz.entity.Role;
import com.htdz.entity.Users;
import com.htdz.entity.UsersRole;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

public interface UsersService {

	/*Users getUserById(Integer id);
	
	void deleteById(Long userId);*/
	
	List<Users> listUsers(Users user,Integer pageNum);
	
	@Transactional //跟Application 中的 @EnableTransactionManagement 组合使用
	void saveUsers(Users users);

	Users login(String usersName,String inputPassword);

	Users getUsers(Map map);

	List<UsersRole> listUsersRole(Map map);

	@Transactional
	void delete(Long usersId);

	Integer checkUsersName(String usersName);
}
