package com.htdz.service.impl;

import com.github.pagehelper.PageHelper;
import com.htdz.dao.UsersMapper;
import com.htdz.dao.UsersRoleMapper;
import com.htdz.entity.Role;
import com.htdz.entity.Users;
import com.htdz.entity.UsersRole;
import com.htdz.service.UsersService;
import com.htdz.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class UsersServiceImpl implements UsersService {

	@Autowired
	private UsersMapper  usersMapper;
	@Autowired
	private UsersRoleMapper usersRoleMapper;
	
	/*@Override
	public Users getUserById(Integer id) {
		Users users = usersMapper.getById(id);
		return users;
	}
	
	@Override
	public void deleteById(Long userId){

		int i = usersMapper.deleteById(userId);
		System.out.println("delete===> "+i);
		
	}*/
	
	@Override
	public List<Users> listUsers(Users users,Integer pageNum){
        PageHelper.startPage(pageNum, Constants.PAGE_SIZE);

        List<Users> usersList = usersMapper.selectAll();

		return usersList;
	}

	@Override
	public void saveUsers(Users users) {

		Map map = new HashMap();
		map.put("usersName",users.getUsersName());
		map.put("pwd",users.getPwd());
		map.put("enabled",users.getEnabled());
		map.put("status",users.getStatus());
		usersMapper.insert(map);
		
	}

	@Override
	public Users login(String usersName, String inputPassword) {
		Map map = new HashMap();
		map.put("usersName",usersName);
		map.put("pwd",inputPassword);
		List<Users> usersList = usersMapper.listBy(map);
		if(usersList.size() == 0){
			return null;
		}else{
			return usersList.get(0);
		}
	}

	//确定只有一条记录
	@Override
	public Users getUsers(Map map) {
		return usersMapper.listBy(map).get(0);
	}

	@Override
	public List<UsersRole> listUsersRole(Map map) {

		return usersRoleMapper.listBy(map);
	}

	@Override
	public void delete(Long usersId) {
		usersMapper.delete(usersId);
		usersRoleMapper.delete(usersId);

	}

	@Override
	public Integer checkUsersName(String usersName) {
		Map map = new HashMap();
		map.put("usersName",usersName);
		List<Users> usersList = usersMapper.listBy(map);
		if(usersList.size() == 0){
			return 0;
		}else{
			return 1;
		}
	}

}
