package com.htdz.service.impl;

import com.htdz.dao.ResourceMapper;
import com.htdz.dao.RoleResourceMapper;
import com.htdz.dao.UsersMapper;
import com.htdz.entity.*;
import com.htdz.service.ResourceService;
import com.htdz.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ResourceServiceImpl implements ResourceService {

	@Autowired
	private ResourceMapper  resourceMapper;
	@Autowired
	private RoleResourceMapper roleResourceMapper;


	@Override
	public List<Resource> listResource(Resource resource, Integer pageNum) {
		Map map = BeanUtil.transBean2Map(resource);
		return resourceMapper.listBy(map);
	}

    /**
     * 列出资源树
     * @param resource
     * @param roleId
     * @return
     */
	@Override
	public List<Node> listResourceTree(Resource resource,Long roleId,Boolean isLeftMenu) {
		Map map = BeanUtil.transBean2Map(resource);
        List<Resource> resourceList = resourceMapper.listBy(map);
        Map roleMap = new HashMap();
        if(roleId == null) roleId = 0L;
        roleMap.put("roleId",roleId);
        List<RoleResource> roleResourceList = roleResourceMapper.listBy(roleMap);
        List<Node> nodeList = new ArrayList<Node>();
        for(Resource re :resourceList){
            Node  node = new Node();
            Boolean isChecked = false;
            for(RoleResource roleResource:roleResourceList){
                if(re.getResourceId() == roleResource.getResourceId()) isChecked = true;
            }
            node.setChecked(isChecked);
            node.setId(re.getResourceId());
            node.setName(re.getResourceName());
            node.setOpen(!isLeftMenu);
            node.setpId(re.getParentId());
            node.setUrl(re.getResourceUrl());
            node.setTarget("_self");

            nodeList.add(node);
        }
		return nodeList;
	}

    /**
     * 列出左边树型菜单
     * @param resource
     * @param usersRoleList
     * @return
     */
    @Override
    public List<Node> listMenuResourceTree(Resource resource, List<UsersRole> usersRoleList) {
        Map map = BeanUtil.transBean2Map(resource);
        List<Resource> resourceList = resourceMapper.listBy(map);
        String roleIds = "";
        for(UsersRole usersRole : usersRoleList){
            roleIds = roleIds + usersRole.getRoleId()+",";
        }
        Map roleMap = new HashMap();
        roleMap.put("roleIds",roleIds);

        List<RoleResource> roleResourceList = roleResourceMapper.listResourceByRoleIds(roleMap);
        List<Node> nodeList = new ArrayList<Node>();
        for(RoleResource roleResource :roleResourceList){
            Node  node = new Node();

            Resource re = resourceMapper.getById(roleResource.getResourceId());
            node.setId(re.getResourceId());
            node.setName(re.getResourceName());
            node.setOpen(false);
            node.setpId(re.getParentId());
            node.setUrl(re.getResourceUrl());
            node.setTarget("_self");

            nodeList.add(node);
        }
        return nodeList;
    }

    @Override
    public void saveRoleResource(Long roleId, String checkedIds) {

        roleResourceMapper.delete(roleId);

        String[] ids = checkedIds.split(",");
        for(String id : ids){
            Map map = new HashMap();
            map.put("roleId",roleId);
            map.put("resourceId",id);
            roleResourceMapper.insert(map);
        }
    }

    @Override
    public Map listMenuResource(List<UsersRole> usersRoleList) {
        Map menuMap = new HashMap<String,List>();
        String roleIds = "";
        for(UsersRole usersRole : usersRoleList){
            roleIds = roleIds + usersRole.getRoleId()+",";
        }
        Map roleMap = new HashMap();
        roleMap.put("roleIds",roleIds);

        List<RoleResource> roleResourceList = roleResourceMapper.listResourceByRoleIds(roleMap);
        List<Resource> resourceTopList = new ArrayList<>();//一级菜单
        List<Resource> resourceSecondList = new ArrayList<>();//二级菜单
        for(RoleResource roleResource :roleResourceList){
            Resource resource = resourceMapper.getById(roleResource.getResourceId());
            if(resource.getParentId() == 0){
                resourceTopList.add(resource);
            }else{
                resourceSecondList.add(resource);
            }
        }
        menuMap.put("resourceTopList",resourceTopList);
        menuMap.put("resourceSecondList",resourceSecondList);
        return menuMap;
    }

    @Override
    public Resource getParentResourceByResourceId(Long resourceId) {
        return resourceMapper.getParentResourceByResourceId(resourceId);
    }
}
