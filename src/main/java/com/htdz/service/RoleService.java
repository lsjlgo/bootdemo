package com.htdz.service;

import com.htdz.entity.Node;
import com.htdz.entity.Resource;
import com.htdz.entity.Role;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RoleService {

	List<Role> listRole(Role role, Integer pageNum);

	List<Node> listRoleTree(Role role,Long usersId);

	@Transactional
	void saveUsersRole(Long usersId,String checkedIds);

	@Transactional
	void saveRole(String roleName);

    @Transactional
	void deleteRole(Long roleId);
}
