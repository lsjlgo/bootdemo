package com.htdz;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

//调用配置job.xml
@Configuration
@ImportResource(locations = {"classpath:job.xml"})
public class JobConfig {
}
