package com.htdz;

import com.htdz.service.UsersService;
import com.htdz.util.CommonRedisUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
//@EnableWebMvc
@Configuration
@SpringBootApplication
@MapperScan(basePackages = {"com.htdz.dao"})//扫描DAO
@EnableAutoConfiguration
@EnableTransactionManagement//开启事物管理
public class Application  {

	@Autowired
	public UsersService usersService;

    @Autowired
    private CommonRedisUtil commonRedisUtil;

	
/*	@RequestMapping("/")
	@ResponseBody
	public String index(){
		return "hello spring boot";
	}*/

	/*@RequestMapping("/")
	public ModelAndView login(){
		ModelAndView result = new ModelAndView("login");
		return result;
	}*/

	@RequestMapping("/")
	public String login(Model model){
		/*try{
			String result = commonRedisUtil.getJedis().get("runoobkey");
			System.out.println(".............result "+ result);
		}catch (Exception e	){
			System.out.println("===========> 请检查redis 中间件异常");
		}*/

		return "login";

	}
	
	/*@RequestMapping("/findUser/{id}")
	public Users findUser(@PathVariable Integer id){
		return usersService.getUserById(id);
	}
	
	@RequestMapping("/getUserById")
	public Users getUserById(Integer id){
		return usersService.getUserById(id);
	}	
	
	@RequestMapping("/deleteById")
	public void deleteById(Long usersId){

		usersService.deleteById(usersId);
	}*/
	
	
/*	@RequestMapping("/getAllUsers")
	//@ResponseBody()
	public String getAllUsers(Users users,Model model){
        ModelAndView result = new ModelAndView("usersList");
        List<Users> usersList = usersService.getAllUsers(users,0);
        result.addObject("usersList", usersList);
        result.addObject("queryParam", users);
        *//*result.addObject("page", users.getPageNum());
        result.addObject("rows", users.getPageSize());*//*
        System.out.println("====> "+new PageInfo<Users>(usersList).getTotal());
        model.addAttribute("usersList", usersList);
        return "usersList";
	}*/

	/*@RequestMapping("/getAllUsers")
	//@ResponseBody()
	public ModelAndView getAllUsers(Users users){
		ModelAndView result = new ModelAndView("allUsersList");
		List<Users> usersList = usersService.listUsers(users,0);
		result.addObject("usersList", usersList);
		result.addObject("queryParam", users);
        *//*result.addObject("page", users.getPageNum());
        result.addObject("rows", users.getPageSize());*//*
		System.out.println("====> "+new PageInfo<Users>(usersList).getTotal());
		return result;
	}
	
	//分页
	@RequestMapping("/getAll")
	public RespData getAll(Users users,Integer pageNum){
        ModelAndView result = new ModelAndView("index");
        System.out.println("================>"+pageNum);
        if(pageNum == null) pageNum = 1;
        List<Users> usersList = usersService.listUsers(users,pageNum);
        result.addObject("pageInfo", new PageInfo<Users>(usersList));
        result.addObject("queryParam", users);
*//*        result.addObject("page", users.getPageNum());
        result.addObject("rows", users.getPageSize());*//*
        System.out.println("====> "+new PageInfo<Users>(usersList).getTotal());
        PageInfo<Users> pageInfo = new PageInfo<Users>(usersList);
        RespData resp = new RespData();//返回格式
        resp.setPageNum(pageNum);
        resp.setPageSize(Constants.PAGE_SIZE);
        resp.setPage(pageInfo.getPages());
        resp.setTotalCount(pageInfo.getTotal());
        resp.setData(usersList);
        
		return resp;
	}	*/
	
/*	@RequestMapping("/users")
	public String users(){
		return "redirect:/users/getAll";
	}*/

	/*@RequestMapping(value = "/all")
	public ModelAndView all(Users users,Integer pageNum){
		ModelAndView view = new ModelAndView("all");
        if(pageNum == null) pageNum = 1;
        List<Users> usersList = usersService.listUsers(users,pageNum);
        view.addObject("pageInfo", new PageInfo<Users>(usersList));
        
		return view;
	}*/

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

		
	}
}
