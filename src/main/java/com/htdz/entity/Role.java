package com.htdz.entity;

import cn.jarod.myframework.gaeacore.base.entity.BaseEntity;

public class Role extends BaseEntity  {

	// 主键
	private long roleId;
	private String roleName;

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
