package com.htdz.entity;

import cn.jarod.myframework.gaeacore.base.entity.BaseEntity;

public class UsersRole extends BaseEntity  {

	// 主键
	private long usersId;
	private long roleId;

	public long getUsersId() {
		return usersId;
	}

	public void setUsersId(long usersId) {
		this.usersId = usersId;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
}
