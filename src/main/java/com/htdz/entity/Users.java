package com.htdz.entity;

import cn.jarod.myframework.gaeacore.base.entity.BaseEntity;
public class Users extends BaseEntity  {

	// 主键
	private long usersId;
	// 用户名
	private String usersName;
	// 密码
	private String pwd;
	// 启用状态
	private int enabled;
	// 状态
	private int status;

	public void setUsersId(long usersId){
		this.usersId=usersId;
	}

	public long getUsersId(){
		return usersId;
	}

	public void setUsersName(String usersName){
		this.usersName=usersName;
	}

	public String getUsersName(){
		return usersName;
	}

	public void setPwd(String pwd){
		this.pwd=pwd;
	}

	public String getPwd(){
		return pwd;
	}

	public void setEnabled(int enabled){
		this.enabled=enabled;
	}

	public int getEnabled(){
		return enabled;
	}

	public void setStatus(int status){
		this.status=status;
	}

	public int getStatus(){
		return status;
	}

}
