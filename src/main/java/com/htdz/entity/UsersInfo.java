package com.htdz.entity;

import java.io.Serializable;

public class UsersInfo implements Serializable {
    private Long usersId;
    private String usersName;

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }

    public String getUsersName() {
        return usersName;
    }

    public void setUsersName(String usersName) {
        this.usersName = usersName;
    }
}
