package com.htdz.entity;

import cn.jarod.myframework.gaeacore.base.entity.BaseEntity;

public class RoleResource extends BaseEntity  {

	// 主键
	private long roleId;
	private long resourceId;

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public long getResourceId() {
		return resourceId;
	}

	public void setResourceId(long resourceId) {
		this.resourceId = resourceId;
	}
}
