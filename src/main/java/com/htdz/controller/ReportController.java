package com.htdz.controller;

import com.github.pagehelper.PageInfo;
import com.htdz.entity.ReportData;
import com.htdz.entity.Users;
import com.htdz.entity.UsersInfo;
import com.htdz.service.RoleService;
import com.htdz.service.UsersService;
import com.htdz.util.JsonUtil;
import com.htdz.util.RedisUtil;
import com.htdz.webservice.cxf.DTSaleArea2CityDistReq;
import com.htdz.webservice.cxf.DTSaleArea2CityDistRes;
import com.htdz.webservice.cxf.SISaleArea2CityDistOut;
import com.htdz.webservice.cxf.SISaleArea2CityDistOutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.BindingProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/rpt")
public class ReportController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	public UsersService usersService;
	@Autowired
	public RedisUtil redisUtil;
	@Autowired
	public RoleService roleService;

	/**
	 * 柱状图
	 * @param request
	 * @param model
	 * @return
	 */
    @RequestMapping("/linkReportBar")
    public String linkReportBar(HttpServletRequest request,Model model){
        return "listReportBar";
    }

    /**
     * 线性图
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("/linkReportLine")
    public String linkReportLine(HttpServletRequest request,Model model){
        return "listReportLine";
    }

    @RequestMapping("/linkReportPie")
    public String linkReportPie(HttpServletRequest request,Model model){
        return "listReportPie";
    }

    @RequestMapping("/linkWorld")
    public String linkWorld(HttpServletRequest request,Model model){
        return "listWorld";
    }

    @RequestMapping("/linkWorldQDH")
    public String linkWorldQDH(HttpServletRequest request,Model model){
        //设置请求参数
        DTSaleArea2CityDistReq dtSaleArea2CityDistReq = new DTSaleArea2CityDistReq();
        dtSaleArea2CityDistReq.setBEGDATE("2017-11-02");

        //服务类
        SISaleArea2CityDistOutService service = new SISaleArea2CityDistOutService();
        SISaleArea2CityDistOut out = service.getHTTPPort();

        //设置webservice用户名密码，接口验证
        BindingProvider bp = (BindingProvider) out;
        Map<String, Object> context = bp.getRequestContext();
        context.put(BindingProvider.USERNAME_PROPERTY, "pisuper");
        context.put(BindingProvider.PASSWORD_PROPERTY, "Transfar2016");

        //调用具体方法名称
        DTSaleArea2CityDistRes dtSaleArea2CityDistRes = out.siSaleArea2CityDistOut(dtSaleArea2CityDistReq);

        List<DTSaleArea2CityDistRes.ITEMS> items  =  dtSaleArea2CityDistRes.getITEMS();

        for(DTSaleArea2CityDistRes.ITEMS item : items){
            System.out.println(item.getBZIRK()+" || "+item.getBZTXT()+" || "+item.getERDATE()+" || "+item.getPID());
        }
        return "listWorldQDH";
    }

    @RequestMapping("/linkChinaQDH")
    public String linkChinaQDH(HttpServletRequest request,Model model){
        return "listChinaQDH";
    }

    @RequestMapping("/linkChina")
    public String linkChina(HttpServletRequest request,Model model){
        return "listChina";
    }

    @RequestMapping("/getReportBar")
	@ResponseBody
	public String getReportBar(HttpServletRequest request,Model model){
		List lists = new ArrayList();
		ReportData data = new ReportData();
		data.setName("一月");
		data.setValue(100);
		lists.add(data);

		ReportData data2 = new ReportData();
		data2.setName("二月");
		data2.setValue(20);
		lists.add(data2);

        ReportData data3 = new ReportData();
        data3.setName("三月");
        data3.setValue(80);
        lists.add(data3);

        ReportData data4 = new ReportData();
        data4.setName("四月");
        data4.setValue(93);
        lists.add(data4);

        ReportData data5 = new ReportData();
        data5.setName("五月");
        data5.setValue(60);
        lists.add(data5);
		return JsonUtil.obj2json(lists);
	}
}
