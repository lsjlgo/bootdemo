package com.htdz.controller;

import com.htdz.constant.AreaEnum;
import com.htdz.entity.ReportData;
import com.htdz.entity.RespData;
import com.htdz.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/tool")
public class ToolbarController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/linkToolbar")
    public String linkToolbar(Model model){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar now = Calendar.getInstance();
        Date startDate = now.getTime();
        model.addAttribute("endDate",sdf.format(startDate));

        now.add(Calendar.DAY_OF_MONTH, -7);
        Date endDate = now.getTime();
        model.addAttribute("startDate",sdf.format(endDate));

        return "listToolbar";
    }

    @RequestMapping("/linkToolbar2")
    public String linkToolbar2(Model model){
        return "listToolbar2";
    }

    @RequestMapping(value="/getCityListByProv")
    @ResponseBody
    public String getCityListByProv(String provCode){
        List lists = new ArrayList();
        RespData data ;

        for (AreaEnum areaEnum : AreaEnum.values()) {
            if (areaEnum.code.startsWith(provCode+"-")) {
                data = new RespData();
                data.setCode(areaEnum.getCode());
                data.setValue(areaEnum.getValue());
                lists.add(data);
            }
        }
        return JsonUtil.obj2json(lists);
    }
}
