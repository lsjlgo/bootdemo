package com.htdz.controller;

import com.github.pagehelper.PageInfo;
import com.htdz.entity.*;
import com.htdz.service.ResourceService;
import com.htdz.service.RoleService;
import com.htdz.service.UsersService;
import com.htdz.util.JsonUtil;
import com.htdz.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/base")
public class ResourceController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	public ResourceService resourceService;
    @Autowired
    public RoleService roleService;
    @Autowired
    public UsersService usersService;
	@Autowired
	public RedisUtil redisUtil;



	@RequestMapping(value = "/listResource")
	public String listResource(HttpServletRequest request,Model model, Integer pageNum){
		if( pageNum == null ) pageNum = 1;
		Resource resource = new Resource();
        List<Resource> resourceList = resourceService.listResource(resource,pageNum);
        List<Role> roleList = roleService.listRole(new Role(),pageNum);
        model.addAttribute("resourceList",resourceList);
        model.addAttribute("roleList",roleList);
        //model.addAttribute("pageInfo",new PageInfo(resourceList));
		return "listResource";
	}

	/**
	 * 列出资源树
	 * @param model
	 * @param roleId
	 * @param isLeftMenu
	 * @return
	 */
	@RequestMapping(value = "/listResourceTree")
	@ResponseBody
	public String listResourceTree(Model model,Long roleId,Boolean isLeftMenu){
		if(isLeftMenu == null) isLeftMenu = false;
		Resource resource = new Resource();
		List<Node> nodeList = resourceService.listResourceTree(resource,roleId,isLeftMenu);
		logger.info(JsonUtil.obj2json(nodeList));
		return JsonUtil.obj2json(nodeList);
	}

	/**
	 * 列出左边菜单树
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/listMenuResourceTree")
	@ResponseBody
	public String listMenuResourceTree(HttpServletRequest request,Model model){
		UsersInfo usersInfo = (UsersInfo)request.getSession().getAttribute("usersInfo");
		Map map = new HashMap<>();
		map.put("usersId",usersInfo.getUsersId());
		List<UsersRole> usersRoleList = usersService.listUsersRole(map);
		//一个用户如有多个角色，取资源的全集
		Resource resource = new Resource();
		List<Node> nodeList = resourceService.listMenuResourceTree(resource,usersRoleList);
		logger.info(JsonUtil.obj2json(nodeList));
		return JsonUtil.obj2json(nodeList);
	}

	@RequestMapping("/saveRoleResource")
	public String saveRoleResource(Model model,Long roleId,String checkedIds){
		resourceService.saveRoleResource(roleId,checkedIds);
		return "redirect:/base/listResource";
	}

}
