package com.htdz.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component//添加此注解可以在其它地方直接通过@Autowired注解引用注入
//以配置文件中的redis.host为开头，直接属性自动绑定
// 如果以@Value注解需读取application.properties文件，并在
//@Value(${"spring,redis.host"})配置文件中的属性名称
//private String host;
@ConfigurationProperties(prefix = "redis")
@PropertySource("classpath:redis.properties")//此注解引用redis.properties配置文件
public class RedisConfig {
    private Integer database;
    private String host;
    private String password;
    private Integer port;
    private Integer timeout;

    public Integer getDatabase() {
        return database;
    }

    public void setDatabase(Integer database) {
        this.database = database;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
}
