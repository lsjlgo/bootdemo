package com.htdz.config;

import com.htdz.filter.LoginInterceptor;
import com.htdz.filter.MenuInterceptor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MyWebAppConfig extends WebMvcConfigurerAdapter implements ApplicationContextAware {

    //不增加此bean，在拦截器中无法注入bean
    @Bean
    LoginInterceptor localInterceptor() {
        return new LoginInterceptor();
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	//在resources下增加其它文件夹，内容必须是静态文件，动态文件不生效
    	//src/main/resource
        //|__________otherImg
    	//

        registry.addResourceHandler("/otherImg/**").addResourceLocations("classpath:/otherImg/");
        super.addResourceHandlers(registry);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //拦截规则：除了login，其他都拦截判断
        registry.addInterceptor(localInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/login")
                .excludePathPatterns("/logout")
                .excludePathPatterns("/")
                .excludePathPatterns("/checkIn");

        //菜单拦截器
        registry.addInterceptor(new MenuInterceptor()).addPathPatterns("/**");

        super.addInterceptors(registry);
    }
}