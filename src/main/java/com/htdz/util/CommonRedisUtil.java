package com.htdz.util;

import com.htdz.config.RedisConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

@Component
public class CommonRedisUtil {
    private static final Logger logger = LoggerFactory.getLogger(CommonRedisUtil.class);

    @Autowired
    private RedisConfig redisConfig;

    private static CommonRedisUtil redisUtil = null;

    private static Jedis jedis;

    public Jedis getJedis() {
        jedis = new Jedis(redisConfig.getHost(),redisConfig.getPort());
        jedis.auth(redisConfig.getPassword());

        return jedis;
    }

}
