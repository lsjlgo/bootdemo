package com.htdz.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;

import java.util.Date;
import java.util.List;

/**
 * JSON工具类
 * 
 * @author tom.zhu
 *
 */
public class JsonUtil {
	
	private static SerializeConfig mapping = new SerializeConfig();
	
	static {
	    mapping.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
	}
	
	/**
	 * 将对象转换成JSON
	 * @param obj
	 * @return
	 */
	public static String obj2json(final Object obj) {
		final Object o = JSON.toJSONString(obj, mapping, SerializerFeature.WriteMapNullValue);
		return o.toString();
	}
	
	/**
	 * 将JSON转换成对象
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> T json2obj(final String json, final Class<T> clazz) {
		final Object o = JSON.parseObject(json, clazz);
		return (T)o;
	}
	
	/**
	 * 普通bean对象转换（包括Map对象）
	 * @param jsonStr json字符
	 * @return 
	 */
	public static Object json2obj(String jsonStr) {
		return JSON.parse(jsonStr);
	}
	
	/**
	 * 将JSON转换成List
	 * @param json
	 * @param clz
	 * @return
	 */
	public static List parseArray(final String json, final Class<?> clz) {
		final List list = JSON.parseArray(json, clz);
		return list;
	}
	
	/**
	 * 获取某一个值
	 * @param json
	 * @param key
	 * @return
	 */
	public static String get(final String json, final String key){
		final JSONObject object = JSON.parseObject(json);
        return object.get(key).toString();
	}
	
	/**
	 * 将对象转换为JsonP
	 * @param callbackName 回调名称
	 * @param object
	 * @return
	 * 
	public String toJsonP(final String callbackName, final Object object) {
		try {
			return mapper.writeValueAsString(new JSONPObject(callbackName, object));
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	*/
}
