package com.htdz.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 用于读取某个指定配置文件，此方法为 /config/public_system.properties
 */
public class PropertiesUtil {
    private static Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);
    private static Properties properties = null;

    public PropertiesUtil() {
    }

    public static String getPropertieValue(String key) {
        String value = "";
        value = properties.getProperty(key).trim();
        return value;
    }

    static {
        properties = new Properties();
        InputStream inputStream = PropertiesUtil.class.getResourceAsStream("/public_system.properties");

        try {
            properties.load(inputStream);
        } catch (IOException io) {
            logger.error("Logger==>PropertiesUtil==>Error:", io);
            io.printStackTrace();
        }

    }
}
