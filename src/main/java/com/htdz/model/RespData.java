package com.htdz.model;

import java.util.ArrayList;
import java.util.List;

public class RespData {

	private int page ;//总的页数
	private int pageNum ;//第几页
	private int pageSize ;//每页显示的记录数
	private long totalCount ;//总的记录数
	
	private List data ;

	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	
	public List<Object> addObject(Object o){
		data.add(o);
		return data;
	}
	public List<Object> getData() {
		return data;
	}
	public void setData(List data) {
		this.data = data;
	}
	
}
