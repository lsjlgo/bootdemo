package com.htdz.filter;

import com.htdz.entity.Node;
import com.htdz.entity.Resource;
import com.htdz.entity.UsersInfo;
import com.htdz.entity.UsersRole;
import com.htdz.service.ResourceService;
import com.htdz.service.UsersService;
import com.htdz.util.RedisUtil;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
public class MenuInterceptor implements HandlerInterceptor {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        logger.info("---------------------开始进入菜单请求地址拦截----------------------------");
        /* 采用ztree做左边树型菜单时的代码
        String parentTId = httpServletRequest.getParameter("parentTId");
        HttpSession session = httpServletRequest.getSession();
        session.setAttribute("parentTId",parentTId);

        logger.info(".................true "+parentTId);*/

        String baseResourceId = httpServletRequest.getParameter("baseResourceId");
        if(baseResourceId != null){
            //baseResourceId 规则 父节点+当前节点
            httpServletRequest.getSession().setAttribute("currentResourceId",baseResourceId.split("-")[1]);
            httpServletRequest.getSession().setAttribute("currentTopResourceId",baseResourceId.split("-")[0]);

        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        logger.info("---------------------请求菜单地址拦截结束----------------------------");
    }

    public void makeResourceForShow(HttpServletRequest request,HttpServletResponse response){

        List<Resource> topResourceList = new ArrayList<Resource>();
        List<Resource> leftResourceList = new ArrayList<Resource>();
       /* SessionOper sessionOper = (SessionOper)request.getSession().getAttribute(Constants.SESSION_OPER);
        Set<Long> resourceIdSet = sessionOper.getResourceIdSet();
        for(Resource resource : Parameters.FIRST_LEVEL_RESOURCE_LIST){
            if(resourceIdSet.contains(resource.getResourceId())){
                topResourceList.add(resource);
            }
        }

        if(topResourceList.isEmpty()){
            request.setAttribute("topResourceList", topResourceList);
            request.setAttribute("leftResourceList", leftResourceList);
            return;
        }*/

       /* //获取当前一级菜单ID
        Long defaultTopResourceId = topResourceList.get(0).getResourceId();
        String sessionStr = (String)request.getSession().getAttribute(Constants.DEFAULT_TOP_RESOURCE);
        if(!StringUtil.isBlank(sessionStr)){
            defaultTopResourceId = Long.parseLong(sessionStr);
        }

        //获取当前一级菜单下的二级菜单ID列表
        List<Long> secondLevelResourceIdList = Parameters.PARENTRESOURCEID_RESOURCEIDLIST_MAP.get(defaultTopResourceId);

        //循环所有二级菜单
        for(Resource resource : Parameters.SECOND_LEVEL_RESOURCE_LIST){
            if(!resourceIdSet.contains(resource.getResourceId())) continue; //过滤掉不属于本操作员的二级菜单
            if(!secondLevelResourceIdList.contains(resource.getResourceId())) continue;//过滤掉不属于当前一级菜单的二级菜单
            resource.setResourceList(new ArrayList<Resource>());
            leftResourceList.add(resource);
        }

        request.setAttribute("topResourceList", topResourceList);
        request.setAttribute("leftResourceList", leftResourceList);*/

        /*UsersInfo usersInfo = (UsersInfo)request.getSession().getAttribute("sessionKey");
        Map map = new HashMap<>();
        map.put("usersId",usersInfo.getUsersId());
        List<UsersRole> usersRoleList = usersService.listUsersRole(map);
        //一个用户如有多个角色，取资源的全集
        Map menuMap = resourceService.listMenuResource(usersRoleList);
        request.setAttribute("menuMap",menuMap);*/
    }
}
