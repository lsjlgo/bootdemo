<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<%=basePath%>bootstrap/favicon.ico">
    <title>后台管理</title>

    <!-- Bootstrap core CSS -->
    <link href="<%=basePath%>bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<%=basePath%>css/zTreeStyle.css" rel="stylesheet">
    <link href="<%=basePath%>bootstrap/validator/bootstrapValidator.css" rel="stylesheet">
    <link href="<%=basePath%>bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="<%=basePath%>bootstrap/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<%=basePath%>bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<%=basePath%>css/common.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<%=basePath%>bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<%=basePath%>bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<%=basePath%>js/jquery-3.3.1.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-paginator.min.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-select.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="<%=basePath%>js/Chart.js"></script>
    <script src="<%=basePath%>js/echarts.js"></script>
    <script src="<%=basePath%>js/jquery.ztree.core.js"></script>
    <script src="<%=basePath%>js/jquery.ztree.excheck.js"></script>
    <script src="<%=basePath%>bootstrap/validator/bootstrapValidator.js"></script>
</head>
<body>
<div class="viewframeContainer">
    <div class="topbar">
        <div class="pull-left">
            <a class="navbar-brand">
                <span class="glyphicon glyphicon-link"></span>
                <span class="text-primary">后台管理</span>
            </a>
        </div>
        <div class="pull-right topbar-user">
            欢迎 ${sessionScope.usersInfo.usersName}&nbsp;|&nbsp;<span id="usersname"></span>
            &nbsp;&nbsp;<a href="#" onclick="logout()"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;安全退出</a>
        </div>
    </div>
</div>
<form name="form1" method="post"></form>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<%=basePath%>bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
<script>
    function logout() {
        document.form1.action="<%=basePath%>logout?usersName=${sessionScope.usersInfo.usersName}";
        document.form1.submit();
    }
</script>
<style>
    [class^="icon-"],
    [class*=" icon-"] {
        display: inline-block;
        width: 14px;
        height: 14px;
        *margin-right: .3em;
        line-height: 14px;
        vertical-align: text-top;
        background-image: url("<%=basePath%>img/glyphicons-halflings.png");
        background-position: 14px 14px;
        background-repeat: no-repeat;
        margin-top: 1px;
    }
    /* White icons with optional class, or on hover/active states of certain elements */
    .icon-white,
    .nav-pills > .active > a > [class^="icon-"],
    .nav-pills > .active > a > [class*=" icon-"],
    .nav-list > .active > a > [class^="icon-"],
    .nav-list > .active > a > [class*=" icon-"],
    .navbar-inverse .nav > .active > a > [class^="icon-"],
    .navbar-inverse .nav > .active > a > [class*=" icon-"],
    .dropdown-menu > li > a:hover > [class^="icon-"],
    .dropdown-menu > li > a:hover > [class*=" icon-"],
    .dropdown-menu > .active > a > [class^="icon-"],
    .dropdown-menu > .active > a > [class*=" icon-"],
    .dropdown-submenu:hover > a > [class^="icon-"],
    .dropdown-submenu:hover > a > [class*=" icon-"] {
        background-image: url("<%=basePath%>img/glyphicons-halflings-white.png");
    }
    .icon-th {
        background-position: -240px 0;
    }
</style>

<jsp:include page="menu.jsp" flush="true"></jsp:include>

<div id="mainframeContainer" class="mainframeContainer">
    <div class="breadcrumb" style="margin-bottom: 0px">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <li><a href="#">基础信息管理</a></li>
        <li class="active">工具栏</li>
    </div>
    <div style="background-color: #aed2ca;height: 36px;" >
        <span class="input-append date start_datetime">
            时间：<input size="10" type="text" value="${startDate}" readonly >
            <span class="add-on"><i class="icon-th"></i></span>
        </span>
        <span class="input-append date end_datetime">
            -- <input size="10" type="text" value="${endDate}" readonly>
            <span class="add-on"><i class="icon-th"></i></span>
        </span>
        <span>
            <select class="selectpicker" data-live-search="true" multiple data-live-search-placeholder="搜索" title="-请选择-">
                <option>Mustard</option>
                <option>Ketchup</option>
                <option>Relish</option>
                <option>Tent</option>
                <option>Flashlight</option>
                <option>Toilet Paper</option>
            </select>

        </span>
        &nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
    </div>
</div>
<script type="text/javascript">
    $(".start_datetime").datetimepicker({
        language : 'zh-CN',
        format: "yyyy-mm-dd",
        minView: 2,//时间选择器最小单位，2，表示最小为日
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right"
    });
    $(".end_datetime").datetimepicker({
        language : 'zh-CN',
        format: "yyyy-mm-dd",
        minView: 2,//时间选择器最小单位，2，表示最小为日
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right"
    });
</script>
</body>
</html>