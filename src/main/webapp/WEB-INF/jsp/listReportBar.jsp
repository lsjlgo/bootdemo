<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<style>
    .chart{
        width: calc(100% - 125px);
    }
</style>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div class="breadcrumb" style="margin-bottom: 0px">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <li><a href="#">基础信息管理</a></li>
        <li class="active">角色</li>
    </div>
    <div style="text-align: right;background-color: #aed2ca" >
        <input type="button" class="btn btn-primary" value="添加角色" onclick="location.href='<%=basePath%>base/linkAddRole'"/>
    </div>
    <div class="chart">
        <canvas id="myChart"></canvas>
    </div>
</div>
<script>
    var ctx = $("#myChart");
    $.post("<%=basePath%>rpt/getReportBar",
        "",
        function(data,status){
            console.log("data="+JSON.stringify(data),"status="+status);
            var arrCount=[];
            var arrName=[];
            var arrBgColor=[];
            var arrCount2=[];
            var arrName2=[];
            var arrBgColor2=[];
            for(var i = 0;i<data.length;i++){
                arrCount.push(data[i].value);
                arrName.push(data[i].name);
                arrBgColor.push("#"+randomColor());
                arrBgColor2.push("#"+randomColor());
                arrCount2.push(data[i].value+10);//设置第二个柱状图数据
                arrName2.push(data[i].name);
            }
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: arrName,//纵向数据
                    datasets: [{
                        label: '# of Votes',
                        data: arrCount,
                        backgroundColor: 'rgb(23, 89, 122)',//设置柱状前景颜色
                        borderColor: 'rgb(255, 255, 255)',
                    },{//设置第二个柱状图参数，如果不需要可删除
                        label: '# of Votes2',
                        data: arrCount2,
                        backgroundColor: 'rgb(255, 234, 12)',
                        borderColor: 'rgb(255, 255, 255)',
                    }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                    "animation": {//在柱形图标上添加数字
                        "duration": 1000,//设置动画效果时间
                        "onComplete": function() {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            ctx.font = Chart.helpers.fontString(16, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function(dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function(bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
                },
            });

        },
        'json'
    );

    function  randomColor(){
        //颜色字符串
        var colorStr="";
        //字符串的每一字符的范围
        var randomArr=['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];
        //产生一个六位的字符串
        for(var i=0;i<6;i++){
            //15是范围上限，0是范围下限，两个函数保证产生出来的随机数是整数
            colorStr+=randomArr[Math.ceil(Math.random()*(15-0)+0)];
        }
        return colorStr;
    }
</script>