<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="<%=basePath%>bootstrap/favicon.ico">

	<title>Signin Template for Bootstrap</title>

	<!-- Bootstrap core CSS -->
	<link href="<%=basePath%>bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="<%=basePath%>bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

	<!-- Custom styles for this template -->

	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script src="<%=basePath%>bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script src="<%=basePath%>bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <script src="<%=basePath%>js/jquery-3.3.1.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-paginator.min.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap.js"></script>

</head>
<body>

<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Header</th>
                <th>Header</th>
                <th>Header</th>
                <th>Header</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="ls" items="${usersList}" varStatus="i">
            <tr>
                <td>${i.index+1}</td>
                <td>${ls.usersId}</td>
                <td>${ls.usersName}</td>
                <td>${ls.status}</td>
                <td>${ls.status}</td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div id="page" style="text-align: right"> <ul id="pageLimit"></ul></div>
</div>
</body>
<script>
 $('#pageLimit').bootstrapPaginator({
     currentPage: 1,//当前的请求页面。
     totalPages: 20,//一共多少页。
     size:"normal",//应该是页眉的大小。
     bootstrapMajorVersion: 3,//bootstrap的版本要求。
     alignment:"right",
     numberOfPages:5,//一页列出多少数据。
     itemTexts: function (type, page, current) {//如下的代码是将页眉显示的中文显示我们自定义的中文。
         switch (type) {
             case "first": return "首页";
             case "prev": return "上一页";
             case "next": return "下一页";
             case "last": return "末页";
             case "page": return page;
         }
      },
     onPageClicked: function(e,originalEvent,type,page){
         alert("page "+page);
         $.ajax(
             {
                 url:'<%=basePath%>users/getAll',
                 type:'POST',
                 data:{'pageNum':page},
                 dataType:'JSON',
                 success:function (callback) {
                 }
           }
        )
     }
});
</script>
</html>