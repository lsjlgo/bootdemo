<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>

<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div class="breadcrumb" style="margin-bottom: 0px">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <li><a href="#">基础信息管理</a></li>
        <li class="active">用户列表</li>
    </div>
        <form id="saveUsers" action="<%=basePath%>users/saveUsers" method="post" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">用户名：</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="usersName" placeholder="必填项"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-3">
                    <button type="submit" class="btn btn-primary">提交</button>
                    <button type="reset" class="btn btn-inverse" onclick="resetAll()">重置</button>
                </div>
            </div>
        </form>
</div>
<script>
    $(function () {
        $('#saveUsers').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                usersName: {
                    message: '用户名验证失败',
                    validators: {
                        notEmpty: {
                            message: '用户名不能为空'
                        },
                        remote:{
                            message:'用户名已存在',
                            url:'<%=basePath%>users/checkUsersName',
                            delay:2000,
                            type: 'POST'
                        }
                    }
                }
            }
        });
    });
    function resetAll() {
        $('#saveUsers').data("bootstrapValidator").resetForm();
    }
</script>