<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div id="myChart" style="height: 100%"></div>
</div>
<script>
    // 路径配置
    require.config({
        paths: {
            echarts: '<%=basePath%>js'
        }
    });
    $.post("<%=basePath%>rpt/getReportBar",
        "",
        function(data,status){
            console.log("data="+JSON.stringify(data),"status="+status);
            var arrName=[];
            for(var i = 0;i<data.length;i++){
                arrName.push(data[i].name);
            }
            // 使用
            require(
                [
                    'echarts',
                    'echarts/chart/pie' // 使用柱状图就加载pie模块，按需加载
                ],
                function (ec) {
                    // 基于准备好的dom，初始化echarts图表
                    var myChart = ec.init(document.getElementById('myChart'));

                    var option = {
                        title : {
                            text: 'echart.js饼图图例',
                            subtext: '纯属虚构',
                            x:'center'
                        },
                        tooltip : {
                            trigger: 'item',
                            formatter: "{a} <br/>{b} : {c} ({d}%)"
                        },
                        legend: {
                            orient : 'vertical',
                            x : 'left',
                            data:arrName
                        },
                        calculable : true,
                        series : [
                            {
                                name:'比例分布',
                                type:'pie',
                                radius : '55%',
                                center: ['50%', '60%'],//data 的数据必须了是 name,value 参数
                                data: data //[{"name":"一月","value":10},{"name":"二月","value":20},{"name":"三月","value":80},{"name":"四月","value":93},{"name":"五月","value":60}]
                            }
                        ]
                    };

                    // 为echarts对象加载数据
                    myChart.setOption(option);
                }
            );

        },
        'json'
    );




</script>