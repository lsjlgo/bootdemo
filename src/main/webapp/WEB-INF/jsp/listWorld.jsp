<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div id="myChart" style="width: 100%;height:100%;"></div>
</div>
<script src="<%=basePath%>js/echarts-map/world.js"></script>
<script>

    var chart = echarts.init(document.getElementById("myChart"));

    /*
        图中相关城市经纬度,根据你的需求添加数据
        关于国家的经纬度，可以用首都的经纬度或者其他城市的经纬度
    */
    var geoCoordMap = {
        '杭州':[120.183315,30.256576],
        'Germany':[13.402393, 52.518569],
        'America':[-100.696295, 33.679979],
        'Bengal':[90.6120420000,23.8525070000],
        'France ':[2.5666760000,48.6899850000],
        'Britain':[-0.126608, 51.208425],
        'Pakistan ':[71.830204,32.3324930000],
        'India':[77.27911,28.5387400000],
        'Switzerland':[7.4304670000,46.9388340000],
        'Australia':[135.779208,-24.3794380000],
        'Singapore':[103.8874940000,1.3176320000],
        'Czech':[14.4950290000,50.0258730000],
        'Japan':[139.710164, 35.706962],
        'Canada':[-112.8803510000,61.6825220000],
        'Vietnam':[105.7548200000,20.8665060000],
        'Turkey':[34.4101030000,38.9194400000],
        'Indonesia':[119.9943270000,-4.2443730000],
        'Italy':[12.6093060000,42.0099510000],
        'Sweden':[14.6574300000,62.2286090000]
    };

    /*
        记录下起点城市和终点城市的名称，以及权重
        数组第一位为终点城市，数组第二位为起点城市，以及该城市的权重，就是图上圆圈的大小
     */

    // 杭州
    var HZData = [
        [{name: '杭州'}, {name: 'Germany',value: 3}],
        [{name: '杭州'}, {name: 'America',value: 4}],
        [{name: '杭州'}, {name: 'Bengal',value: 3}],
        [{name: '杭州'}, {name: 'France ',value: 2}],
        [{name: '杭州'}, {name: 'Britain',value: 3}],
        [{name: '杭州'}, {name: 'Pakistan ',value: 2}],
        [{name: '杭州'}, {name: 'India',value: 3}],
        [{name: '杭州'}, {name: 'Switzerland',value: 1}],
        [{name: '杭州'}, {name: 'Australia',value: 1}],
        [{name: '杭州'}, {name: 'Singapore',value: 2}],
        [{name: '杭州'}, {name: 'Czech',value: 1}],
        [{name: '杭州'}, {name: 'Japan',value: 1}],
        [{name: '杭州'}, {name: 'Canada',value: 1}],
        [{name: '杭州'}, {name: 'Vietnam',value: 1}],
        [{name: '杭州'}, {name: 'Turkey',value: 1}],
        [{name: '杭州'}, {name: 'Indonesia',value: 2}],
        [{name: '杭州'}, {name: 'Italy',value: 1}],
        [{name: '杭州'}, {name: 'Sweden',value: 1}]
    ];
    // 小飞机的图标，可以用其他图形替换
    var planePath = 'path://M1705.06,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705.06,1318.313z';

    // 获取地图中起点和终点的坐标，以数组形式保存下来
    var convertData = function(data) {
        var res = [];
        for(var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            var fromCoord = geoCoordMap[dataItem[1].name];
            var toCoord = geoCoordMap[dataItem[0].name];
            if(fromCoord && toCoord) {
                res.push([{
                    coord: fromCoord // 起点坐标
                }, {
                    coord: toCoord // 终点坐标
                }])
            }
        }
        return res;
    }

    //var color  = [ '#53dc1b','#ff291c'];    // 自定义图中要用到的颜色
    var color  = [ '#ff291c','#ff291c'];
    var series = [];                        // 用来存储地图数据

    /*
        图中一共用到三种效果，分别为航线特效图、飞机航线图以及城市图标涟漪图。
        要用到setOption中的series属性，并且对每个城市都要进行三次设置。
    */
    [['杭州', HZData]].forEach(function(item, i) {
        series.push({
            // 白色航线特效图
            type: 'lines',
            zlevel: 1,                    // 用于分层，z-index的效果
            effect: {
                show: true,               // 动效是否显示
                period: 6,                // 特效动画时间
                trailLength: 0.7,         // 特效尾迹的长度
                color: '#fff',            // 特效颜色
                symbolSize: 3             // 特效大小
            },
            lineStyle: {
                normal: {                 // 正常情况下的线条样式
                    color: color[0],
                    width: 0,             // 因为是叠加效果，要是有宽度，线条会变粗，白色航线特效不明显
                    curveness: -0.2       // 线条曲度
                }
            },
            data: convertData(item[1])    // 特效的起始、终点位置
        }, {  // 小飞机航线效果
            type: 'lines',
            zlevel: 2,
            //symbol: ['none', 'arrow'],   // 用于设置箭头
            symbolSize: 10,
            effect: {
                show: true,
                period: 6,
                trailLength: 0,
                symbol: planePath,         // 特效形状，可以用其他svg pathdata路径代替
                symbolSize: 15
            },
            lineStyle: {
                normal: {
                    color: color[0],
                    width: 1,
                    opacity: 0.6,
                    curveness: -0.2
                }
            },
            data: convertData(item[1])     // 特效的起始、终点位置，一个二维数组，相当于coords: convertData(item[1])
        }, { // 散点效果
            type: 'effectScatter',
            coordinateSystem: 'geo',       // 表示使用的坐标系为地理坐标系
            zlevel: 3,
            rippleEffect: {
                brushType: 'stroke'        // 波纹绘制效果
            },
            label: {
                normal: {                  // 默认的文本标签显示样式
                    show: true,
                    position: 'left',      // 标签显示的位置
                    formatter: '{b}'       // 标签内容格式器
                }
            },
            itemStyle: {
                normal: {
                    color: color[0]
                }
            },
            data: item[1].map(function(dataItem) {
                return {
                    name: dataItem[1].name,
                    value: geoCoordMap[dataItem[1].name],  // 起点的位置
                    symbolSize: dataItem[1].value * 5,  // 散点的大小，通过之前设置的权重来计算，val的值来自data返回的value
                };
            })
        });
    });

    // 显示终点位置,类似于上面最后一个效果，放在外面写，是为了防止被循环执行多次
    series.push({
        type: 'effectScatter',
        coordinateSystem: 'geo',
        zlevel: 3,
        rippleEffect: {
            brushType: 'stroke'
        },
        label: {
            normal: {
                show: true,
                position: 'left',
                formatter: '{b}'
            }
        },
        symbolSize: function(val) {
            return val[2] / 8;
        },
        itemStyle: {
            normal: {
                color: color[1]
            }
        },
        data: [ {
            name: '杭州',
            value: [120.209761,30.290012,20*5]
        }]
    });

    // 最后初始化世界地图中的相关数据
    chart.setOption({
        title: {
            text: '',//主标题
            subtext: '',//子标题
            textStyle: {
                color: '#33ff32',//标题颜色
                fontSize: 30//标题大小
            },
            left: 'center'
        },
        geo: {
            map: 'world',       // 与引用进来的地图js名字一致
            roam: true,        // 禁止缩放平移
            itemStyle: {        // 每个区域的样式
                normal: {
                    areaColor: '#f1f4ff'  //地图背景色
                },
                emphasis: {
                    areaColor: '#2a333d'
                }
            },
            regions: [{        // 选中的区域
                name: 'China',
                selected: true,
                itemStyle: {   // 高亮时候的样式
                    emphasis: {
                        areaColor: '#7d7d7d'
                    }
                },
                label: {    // 高亮的时候不显示标签
                    emphasis: {
                        show: false
                    }
                }
            }]
        },
        series: series,   // 将之前处理的数据放到这里
        textStyle: {
            fontSize: 12
        }
    });


</script>