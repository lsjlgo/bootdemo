<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div class="breadcrumb" style="margin-bottom: 0px">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <li><a href="#">基础信息管理</a></li>
        <li class="active">用户列表</li>
    </div>
    <div class="leftArea">
        <div class="self-toolbar" >
            &nbsp;&nbsp;
            <a href="#" onclick="location.href='<%=basePath%>users/linkAddUsers'" title="添加用户"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i></a>
        </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>用户名</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="ls" items="${usersList}" varStatus="i">
            <tr>
                <td>${i.index+1}</td>
                <td>${ls.usersName}</td>
                <td>${ls.status}</td>
                <td>
                    <a href="#" onclick="viewGrant('${ls.usersId}',this)" title="查看角色"><i class="glyphicon glyphicon-zoom-in" aria-hidden="true"></i></a>
                    <c:if test="${ls.usersName != 'admin'}">
                    <a href="<%=basePath%>users/deleteUsers?usersId=${ls.usersId}" title="删除"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></a>
                    </c:if>
                </td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div id="page" style="text-align: right"> <ul id="pageLimit"></ul></div>
    </div>
    <div class="rightArea">
        <h5>当前用户：<span id="roleName" class="roleName">请选择用户</span></h5>
        <ul id="treeDemo" class="ztree"></ul>
        <br/>
        <input type="button" value="授权" onclick="grant()">
        <input type="button" value="重置">
    </div>
</div>
<form name="roleForm" id="roleForm">
    <input type="hidden" name="grantUsersId" id="grantUsersId">
    <input type="hidden" name="checkedIds" id="checkedIds">
</form>
<script>
 $('#pageLimit').bootstrapPaginator({
     currentPage: ${pageInfo.pageNum},//当前的请求页面。
     totalPages: ${pageInfo.pages},//一共多少页。
     size:"normal",//应该是页眉的大小。
     bootstrapMajorVersion: 3,//bootstrap的版本要求。
     alignment:"right",
     shouldShowPage:true,
     numberOfPages:${pageInfo.pageSize},//一页列出多少数据。
     itemTexts: function (type, page, current) {//如下的代码是将页眉显示的中文显示我们自定义的中文。
         switch (type) {
             case "first": return "首页";
             case "prev": return "上一页";
             case "next": return "下一页";
             case "last": return "末页";
             case "page": return page;
         }
      },
     onPageClicked: function (e,originalEvent,type,page) {
         location.href = '<%=basePath%>users/listUsers?usersId=${usersInfo.usersId}&pageNum='+page;
     }
});
 var setting = {
     async: {
         enable: true,
         url:"<%=basePath%>base/listRoleTree",
         otherParam:{"usersId":"2","grantUsersId":function(){return $("#grantUsersId").val()}},
         dataFilter: filter,
         type: "get",
         dataType : "json"
     },
     callback : {
         onAsyncSuccess: zTreeOnAsyncSuccess //异步加载完成调用
     },
     check: {
         enable: true
     },
     data: {
         simpleData: {
             enable: true
         }
     }
 };
 function zTreeOnAsyncSuccess(event, treeId, msg) {
 }
 function filter(treeId, parentNode, responseData) {
     return responseData;
 };
 function grant(){
     if($("#grantUsersId").val() == "") return false;
     var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
     var nodes = treeObj.getCheckedNodes(true);
     var checkedIds = null;
     for(var i=0;i<nodes.length;i++){
         checkedIds = checkedIds + nodes[i].id + ",";
     }
     var changeCount = treeObj.getChangeCheckedNodes().length;
     if(changeCount != 0){
         $("#checkedIds").val(checkedIds);
         $("#roleForm").attr("action","<%=basePath%>base/saveUsersRole");
         $("#roleForm").submit();
     }

 }
 function viewGrant(usersId,obj) {
     $("#grantUsersId").val(usersId);
     $("#roleName").html($(obj).parent().parent().find("td").eq(1).text());
     $.fn.zTree.init($("#treeDemo"), setting);
 }
 $(document).ready(function(){
     $.fn.zTree.init($("#treeDemo"), setting);
 });
</script>