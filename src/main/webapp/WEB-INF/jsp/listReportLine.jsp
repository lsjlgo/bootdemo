<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <canvas id="myChart"></canvas>
</div>
<script>
    var ctx = $("#myChart");
    $.post("<%=basePath%>rpt/getReportBar",
        "",
        function(data,status){
            console.log("data="+JSON.stringify(data),"status="+status);
            var arrCount=[];
            var arrName=[];
            var arrBgColor=[];
            var arrCount2=[];
            var arrName2=[];
            var arrBgColor2=[];
            for(var i = 0;i<data.length;i++){
                arrCount.push(data[i].value);
                arrName.push(data[i].name);
                arrBgColor.push("#"+randomColor());
                arrBgColor2.push("#"+randomColor());
                arrCount2.push(data[i].value+10);//设置第二个柱状图数据
                arrName2.push(data[i].name);
            }
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: arrName,//纵向数据
                    datasets: [{
                        label: '# of Votes',
                        data: arrCount,
                        backgroundColor: 'rgb(23, 89, 122)',
                        borderColor: 'rgb(122, 12, 22)',//设置线颜色
                        fill:false,//设置下图是否填充
                        //steppedLine:true,设置矩形图样(阶梯形状)
                        pointRadius: 10,//设置圆点大小
                        borderDash: [5, 5],//设置虚线
                    },{//设置第二个图参数，如果不需要可删除
                        label: '# of Votes2',
                        data: arrCount2,
                        backgroundColor: 'rgb(255, 234, 12)',
                        borderColor: 'rgb(78, 255, 22)',
                        fill:false,
                        pointRadius: 15,
                        //steppedLine:true,
                    }
                    ]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: '折线图示例'//设置标题
                    }
                }
            });

        },
        'json'
    );

    function  randomColor(){
        //颜色字符串
        var colorStr="";
        //字符串的每一字符的范围
        var randomArr=['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];
        //产生一个六位的字符串
        for(var i=0;i<6;i++){
            //15是范围上限，0是范围下限，两个函数保证产生出来的随机数是整数
            colorStr+=randomArr[Math.ceil(Math.random()*(15-0)+0)];
        }
        return colorStr;
    }
</script>