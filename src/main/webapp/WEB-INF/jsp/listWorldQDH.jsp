<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div id="myChart" style="width: 100%;height:100%;"></div>
</div>
<script src="<%=basePath%>js/echarts-map/world.js"></script>
<script>

    var chart = echarts.init(document.getElementById("myChart"));

    /*
        图中相关城市经纬度,根据你的需求添加数据
        关于国家的经纬度，可以用首都的经纬度或者其他城市的经纬度
    */
    var geoCoordMap = {
        '杭州(43)':[120.183315,30.256576],
        '香港(2)':[114.189338,22.279175],
        '孟加拉(3)':[90.6120420000,23.8525070000],
        '印度(6)':[77.27911,28.5387400000],
        '瑞典(1)':[7.4304670000,46.9388340000],
        '德国(1)':[13.402393, 52.518569],
        '巴基斯坦(2)':[71.830204,32.3324930000],
        '越南(1)':[105.7548200000,20.8665060000],
        '印尼(1)':[119.9943270000,-4.2443730000]
    };

    /*
        记录下起点城市和终点城市的名称，以及权重，就是图上圆圈的大小
     */

    // 杭州
    var HZData = [
        [{name: '杭州(43)',value: 40}],
        [{name: '印度(6)',value: 20}],
        [{name: '香港(2)',value: 10}],
        [{name: '孟加拉(3)',value: 15}],
        [{name: '瑞典(1)',value: 5}],
        [{name: '巴基斯坦(2)',value: 10}],
        [{name: '德国(1)',value: 5}],
        [{name: '越南(1)',value: 5}],
        [{name: '印尼(1)',value: 5}]

    ];

    // 小飞机的图标，可以用其他图形替换
    var planePath = 'path://M1705.06,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705.06,1318.313z';

    // 获取地图中坐标和value以数组形式保存下来
    var convertData = function (data) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var geoCoord = geoCoordMap[item[0].name];
            if (geoCoord) {
                res.push({
                    name: item[0].name,
                    value: geoCoord.concat(item[0].value)
                });
            }
        }
        return res;
    }

    var color  = [ '#ff291c','#ff291c'];    // 自定义图中要用到的颜色
    //var color  = [ '#53dc1b','#ff291c'];    // 自定义图中要用到的颜色
    var series = [];                        // 用来存储地图数据

    /*
        图中一共用到三种效果，分别为航线特效图、飞机航线图以及城市图标涟漪图。
        要用到setOption中的series属性，并且对每个城市都要进行三次设置。
    */
    [HZData].forEach(function(item, i) {
        //散点效果+圆点效果，两者效果重叠
        series.push( { // 散点效果
            type: 'effectScatter',
            coordinateSystem: 'geo',       // 表示使用的坐标系为地理坐标系
            zlevel: 3,
            rippleEffect: {
                brushType: 'stroke'        // 波纹绘制效果
            },
            label: {
                normal: {                  // 默认的文本标签显示样式
                    show: true,          // 是否显示标签名
                    position: 'left',      // 标签显示的位置
                    formatter: '{b}',       // 标签内容格式器
                    textStyle:{
                        fontSize:18        //设置字体大小，如果默认，直接去掉textStyle
                    }
                }
            },
            itemStyle: {
                normal: {
                    color: color[0]
                }
            },
            //如果不需要排序，则直接下行语句就行
            //data: convertData(item),
            data: convertData(item.sort(function (a, b) {
                return b.value - a.value;
            })),
            symbolSize: function (val) {
                return val[2];
            }
        },{ // 圆点效果(没有动图)
            type: 'scatter',
            coordinateSystem: 'geo',       // 表示使用的坐标系为地理坐标系
            zlevel: 3,
            rippleEffect: {
                brushType: 'stroke'        // 波纹绘制效果
            },
            label: {
                normal: {                  // 默认的文本标签显示样式
                    show: false,          // 是否显示标签名
                    position: 'left',      // 标签显示的位置
                    formatter: '{b}'       // 标签内容格式器
                }
            },
            itemStyle: {
                normal: {
                    color: color[0]
                }
            },
            data: convertData(item),
            symbolSize: function (val) {
                return val[2] / 10;
            }
        });
    });

    // 显示终点位置,类似于上面最后一个效果，放在外面写，是为了防止被循环执行多次
    series.push({
        type: 'effectScatter',
        coordinateSystem: 'geo',
        zlevel: 3,
        rippleEffect: {
            brushType: 'stroke'
        },
        label: {
            normal: {
                show: true,
                position: 'left',
                formatter: '{b}'
            }

        },
        symbolSize: function(val) {
            return val[2] / 8;
        },
        itemStyle: {
            normal: {
                color: color[1]
            }
        }
    });

    // 最后初始化世界地图中的相关数据
    chart.setOption({
        title: {
            textStyle: {
                color: '#33ff32',//标题颜色
                fontSize: 30//标题大小
            },
            top: 10,
            left: 'center'
        },
        tooltip : {//鼠标移到圆点上显示出的内容
            trigger: 'item',
            formatter: function(item){//默认此函数不需要写，如写可设计成自己需要的格式
                //console.log(item);
                return item.name +" : "+item.value[2] +" 人 ";
            }  //http://echarts.baidu.com/option3.html#tooltip.formatter 用法
        },
        geo: {
            map: 'world',       // 与引用进来的地图js名字一致
            roam: true,        // 禁止缩放平移
            itemStyle: {        // 每个区域的样式
                normal: {
                    areaColor: '#f1f4ff'  //地图背景色
                },
                emphasis: {
                    areaColor: '#2a333d'
                }
            },
            regions: [{        // 选中的区域
                name: 'China',
                selected: false,
                itemStyle: {   // 高亮时候的样式
                    emphasis: {
                        areaColor: '#7d7d7d'
                    }
                },
                label: {    // 高亮的时候不显示标签
                    emphasis: {
                        show: false
                    }
                }
            }]
        },
        series: series,   // 将之前处理的数据放到这里
        textStyle: {
            fontSize: 12 //显示字体大小
        }
    });


</script>