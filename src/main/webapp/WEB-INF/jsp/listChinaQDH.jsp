<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div id="myChart" style="width: 100%;height:100%;"></div>
</div>
<script src="<%=basePath%>js/echarts-map/china.js"></script>
<script>

    var chart = echarts.init(document.getElementById("myChart"));

    /*
        图中相关城市经纬度,根据你的需求添加数据
        关于国家的经纬度，可以用首都的经纬度或者其他城市的经纬度
    */
    var geoCoordMap = {
        '杭州':[120.190214,30.273046],
        '上海':[121.463293,31.245315],
        '香港':[114.189338,22.279175],
        '大连':[121.608405,38.918055],
        '济南':[117.080406,36.667671],
        '湖州':[120.161783,30.865244],
        '枣阳':[112.781924,32.082857],
        '武汉':[114.333433,30.589514],
        '东阳':[120.218528,29.281244],
        '天津':[117.20722,39.138458],
        '邵东':[111.729218,27.254232],
        '昆明':[102.848427,24.885537],
        '苏州':[120.601036,31.314425],
        '南京':[118.785749,32.061714],
        '黄冈':[114.872165,30.472308],
        '深圳':[114.075475,22.553796],
        '广州':[113.276028,23.142248],
        '绍兴':[120.60215,30.037371],
        '义乌':[120.077558,29.301575],
        '松阳':[119.496022,28.447435],
        '重庆':[106.548663,29.562462],
        '嘉兴':[120.778497,30.738559],
        '平遥':[112.183608,37.188699],
        '临汾':[111.505405,36.086276],
        '长春':[125.340955,43.824868],
        '金华':[119.665514,29.09373],
        '丽水':[119.933595,28.450922],
        '温州':[120.690956,27.999555],
        '台北':[121.551892,25.04166],
        '西安':[108.961988,34.351561],
        '江门':[113.087977,22.574993],
        '无锡':[120.313408,31.506199],
        '泉州':[118.674395,24.878379],
        '青岛':[120.406131,36.10256],
        '石家庄':[114.51578,38.054677],
        '北京':[116.415112,39.921992],
        '本溪':[123.707458,41.494644],
        '台州':[121.433181,28.667771],
        '鄂州':[114.91081,30.399561],
        '宁波':[121.623973,29.875054],
        '兰州':[103.837649,36.058832],
        '长沙':[112.940296,28.22827],
        '澳门':[113.53221,22.13248]
    };

    /*
        记录下起点城市和终点城市的名称，以及权重，就是图上圆圈的大小
     */

    // 杭州
    var HZData = [
        [{name: '杭州',value: 15}],
        [{name: '上海',value: 5}],
        [{name: '香港',value: 7}],
        [{name: '大连',value: 1}],
        [{name: '济南',value: 3}],
        [{name: '湖州',value: 3}],
        [{name: '枣阳',value: 1}],
        [{name: '武汉',value: 4}],
        [{name: '东阳',value: 1}],
        [{name: '天津',value: 1}],
        [{name: '邵东',value: 1}],
        [{name: '昆明',value: 1}],
        [{name: '苏州',value: 4}],
        [{name: '南京',value: 4}],
        [{name: '黄冈',value: 1}],
        [{name: '深圳',value: 2}],
        [{name: '广州',value: 2}],
        [{name: '绍兴',value: 5}],
        [{name: '义乌',value: 1}],
        [{name: '松阳',value: 1}],
        [{name: '重庆',value: 1}],
        [{name: '嘉兴',value: 2}],
        [{name: '平遥',value: 1}],
        [{name: '临汾',value: 1}],
        [{name: '长春',value: 1}],
        [{name: '金华',value: 2}],
        [{name: '丽水',value: 1}],
        [{name: '温州',value: 1}],
        [{name: '台北',value: 1}],
        [{name: '西安',value: 1}],
        [{name: '江门',value: 1}],
        [{name: '无锡',value: 1}],
        [{name: '泉州',value: 1}],
        [{name: '青岛',value: 1}],
        [{name: '石家庄',value: 1}],
        [{name: '北京',value: 1}],
        [{name: '本溪',value: 1}],
        [{name: '台州',value: 1}],
        [{name: '鄂州',value: 1}],
        [{name: '宁波',value: 2}],
        [{name: '兰州',value: 1}],
        [{name: '长沙',value: 1}],
        [{name: '澳门',value: 1}]
    ];

    // 小飞机的图标，可以用其他图形替换
    var planePath = 'path://M1705.06,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705.06,1318.313z';

    // 获取地图中坐标和value以数组形式保存下来
    var convertData = function (data) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var geoCoord = geoCoordMap[item[0].name];
            if (geoCoord) {
                res.push({
                    name: item[0].name,
                    value: geoCoord.concat(item[0].value)
                });
            }
        }
        return res;
    }

    var color  = [ '#ff291c','#ff291c'];    // 自定义图中要用到的颜色
    //var color  = [ '#53dc1b','#ff291c'];    // 自定义图中要用到的颜色
    var series = [];                        // 用来存储地图数据

    /*
        图中一共用到三种效果，分别为航线特效图、飞机航线图以及城市图标涟漪图。
        要用到setOption中的series属性，并且对每个城市都要进行三次设置。
    */
    [HZData].forEach(function(item, i) {
        //散点效果+圆点效果，两者效果重叠
        series.push( { // 散点效果
            type: 'effectScatter',
            coordinateSystem: 'geo',       // 表示使用的坐标系为地理坐标系
            zlevel: 3,
            rippleEffect: {
                brushType: 'stroke'        // 波纹绘制效果
            },
            label: {
                normal: {                  // 默认的文本标签显示样式
                    show: false,          // 是否显示标签名
                    position: 'left',      // 标签显示的位置
                    formatter: '{b}'       // 标签内容格式器
                }
            },
            itemStyle: {
                normal: {
                    color: color[0]
                }
            },
            //如果不需要排序，则直接下行语句就行
            //data: convertData(item),
            data: convertData(item.sort(function (a, b) {
                return b.value - a.value;
            })),
            symbolSize: function (val) {
                return val[2] * 2;
            }
        },{ // 圆点效果(没有动图)
            type: 'scatter',
            coordinateSystem: 'geo',       // 表示使用的坐标系为地理坐标系
            zlevel: 3,
            rippleEffect: {
                brushType: 'stroke'        // 波纹绘制效果
            },
            label: {
                normal: {                  // 默认的文本标签显示样式
                    show: false,          // 是否显示标签名
                    position: 'left',      // 标签显示的位置
                    formatter: '{b}'       // 标签内容格式器
                }
            },
            itemStyle: {
                normal: {
                    color: color[0]
                }
            },
            data: convertData(item),
            symbolSize: function (val) {
                return val[2] / 10;
            }
        });
    });

    // 显示终点位置,类似于上面最后一个效果，放在外面写，是为了防止被循环执行多次
    series.push({
        type: 'effectScatter',
        coordinateSystem: 'geo',
        zlevel: 3,
        rippleEffect: {
            brushType: 'stroke'
        },
        label: {
            normal: {
                show: true,
                position: 'left',
                formatter: '{b}'
            }
        },
        symbolSize: function(val) {
            return val[2] / 8;
        },
        itemStyle: {
            normal: {
                color: color[1]
            }
        }
    });

    // 最后初始化世界地图中的相关数据
    chart.setOption({
        title: {
            textStyle: {
                color: '#33ff32',//标题颜色
                fontSize: 30//标题大小
            },
            top: 10,
            left: 'center'
        },
        tooltip : {//鼠标移到圆点上显示出的内容
            trigger: 'item',
            formatter: function(item){//默认此函数不需要写，如写可设计成自己需要的格式
                //console.log(item);
                return item.name +" : "+item.value[2] +" 人 ";
            }  //http://echarts.baidu.com/option3.html#tooltip.formatter 用法
        },
        geo: {
            map: 'china',       // 与引用进来的地图js名字一致
            roam: true,        // 禁止缩放平移
            itemStyle: {        // 每个区域的样式
                normal: {
                    areaColor: '#f1f4ff'  //地图背景色
                },
                emphasis: {
                    areaColor: '#2a333d'
                }
            },
            regions: [{        // 选中的区域
                name: '浙江',
                selected: true,
                itemStyle: {   // 高亮时候的样式
                    emphasis: {
                        areaColor: '#7d7d7d'
                    }
                },
                label: {    // 高亮的时候不显示标签
                    emphasis: {
                        show: false
                    }
                }
            }]
        },
        series: series,   // 将之前处理的数据放到这里
        textStyle: {
            fontSize: 12 //显示字体大小
        }
    });


</script>