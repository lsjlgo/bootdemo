<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div class="breadcrumb" style="margin-bottom: 0px">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <li><a href="#">基础信息管理</a></li>
        <li class="active">角色</li>
    </div>
    <div class="self-toolbar" >
        &nbsp;&nbsp;
        <a href="#" onclick="location.href='<%=basePath%>base/linkAddRole'" title="添加角色"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i></a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>角色名</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="ls" items="${roleList}" varStatus="i">
            <tr>
                <td>${i.index+1}</td>
                <td>${ls.roleName}</td>
                <td><a href="<%=basePath%>base/deleteRole?roleId=${ls.roleId}" title="删除"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></a></td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div id="page" style="text-align: right"> <ul id="pageLimit"></ul></div>
</div>