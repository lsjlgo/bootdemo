<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>Spring Boot Sample</title>
</head>
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
<script language="javascript">
	function turnPage(currPageNum, searchForm){
		$(searchForm).find("input[name='pageNum']").remove();
		$(searchForm).append($("<input type='hidden' name='pageNum'/>").val(currPageNum));
		$(searchForm).submit();
	}
</script>
<body>
	<form id="mainForm">
	</form>
	<table class="table-list">
	<thead>
	  <tr><th>#</th><th>id</th><th>usersName</th><th>deptId</th></tr>
	</thead>
	<c:forEach var="ls" items="${pageInfo.list}" varStatus="i">
	  <tr><td>${i.index+1+(pageInfo.pageNum-1)*pageInfo.pageSize}</td><td>${ls.id}</td><td>${ls.usersName}</td><td>${ls.deptId}</td></tr>
	</c:forEach>
	
	</table>
    <tag:page pageNum="${pageInfo.pageNum}" pageSize="${pageInfo.pageSize}" totalCount="${pageInfo.total}" searchForm="#mainForm"/>
</body>
</html>