<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<div id="mainframeContainer" class="mainframeContainer">
    <div class="breadcrumb" style="margin-bottom: 0px">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <li><a href="#">基础信息管理</a></li>
        <li class="active">授权</li>
    </div>
    <div class="leftArea">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>角色</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="ls" items="${roleList}" varStatus="i">
                    <tr>
                        <td>${i.index+1}</td>
                        <td>${ls.roleName}</td>
                        <td><a href="#" onclick="viewGrant('${ls.roleId}',this)" title="查看权限"><i class="glyphicon glyphicon-zoom-in" aria-hidden="true"></i></a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div class="rightArea">
        <h5>当前角色：<span id="roleName" class="roleName">请选择角色</span></h5>
        <ul id="treeDemo" class="ztree"></ul>
        <br/>
        <input type="button" value="授权" onclick="grant()">
        <input type="button" value="重置">
    </div>
</div>
<form name="resourceForm" id="resourceForm">
    <input type="hidden" name="roleId" id="roleId">
    <input type="hidden" name="checkedIds" id="checkedIds">
</form>
<SCRIPT type="text/javascript">
    var setting = {
        async: {
            enable: true,
            url:"<%=basePath%>base/listResourceTree",
            otherParam:{"usersId":"2","roleId":function(){return $("#roleId").val()}},
            dataFilter: filter,
            type: "get",
            dataType : "json"
        },
        callback : {
            onAsyncSuccess: zTreeOnAsyncSuccess //异步加载完成调用
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        }
    };
    function zTreeOnAsyncSuccess(event, treeId, msg) {
        //var treeObj = $.fn.zTree.getZTreeObj(treeId);
        //treeObj.expandAll(true);
    }
    function filter(treeId, parentNode, responseData) {
       // responseData = responseData.jsonArray;
        return responseData;
    };
    function grant(){
        if($("#roleId").val() == "") return false;
        var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
        var nodes = treeObj.getCheckedNodes(true);
        var checkedIds = null;
        for(var i=0;i<nodes.length;i++){
            checkedIds = checkedIds + nodes[i].id + ",";
        }
        var changeCount = treeObj.getChangeCheckedNodes().length;
        if(changeCount != 0){
            $("#checkedIds").val(checkedIds);
            $("#resourceForm").attr("action","<%=basePath%>base/saveRoleResource");
            $("#resourceForm").submit();
        }

    }
    function viewGrant(roleId,obj) {
        $("#roleId").val(roleId);
        $("#roleName").html($(obj).parent().parent().find("td").eq(1).text());
        $.fn.zTree.init($("#treeDemo"), setting);
    }
    $(document).ready(function(){
        $.fn.zTree.init($("#treeDemo"), setting);
    });
</SCRIPT>