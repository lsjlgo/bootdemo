<%@ page import="com.htdz.entity.UsersInfo" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<%=basePath%>bootstrap/favicon.ico">
    <title>后台管理</title>

    <!-- Bootstrap core CSS -->
    <link href="<%=basePath%>bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<%=basePath%>css/zTreeStyle.css" rel="stylesheet">
    <link href="<%=basePath%>bootstrap/validator/bootstrapValidator.css" rel="stylesheet">
    <link href="<%=basePath%>bootstrap/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="<%=basePath%>bootstrap/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<%=basePath%>bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<%=basePath%>css/common.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<%=basePath%>bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<%=basePath%>bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<%=basePath%>js/jquery-3.3.1.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-paginator.min.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-select.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<%=basePath%>bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="<%=basePath%>js/Chart.js"></script>
    <script src="<%=basePath%>js/echarts.min.js"></script>
    <script src="<%=basePath%>js/jquery.ztree.core.js"></script>
    <script src="<%=basePath%>js/jquery.ztree.excheck.js"></script>
    <script src="<%=basePath%>bootstrap/validator/bootstrapValidator.js"></script>
</head>
<body>
<div class="viewframeContainer">
    <div class="topbar">
        <div class="pull-left">
            <a class="navbar-brand">
                <span class="glyphicon glyphicon-link"></span>
                <span class="text-primary">后台管理</span>
            </a>
        </div>
        <div class="pull-right topbar-user">
            欢迎 ${sessionScope.usersInfo.usersName}&nbsp;|&nbsp;<span id="usersname"></span>
            &nbsp;&nbsp;<a href="#" onclick="logout()"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;安全退出</a>
        </div>
    </div>
</div>
<form name="form1" method="post"></form>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<%=basePath%>bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
<script>
    function logout() {
        document.form1.action="<%=basePath%>logout?usersName=${sessionScope.usersInfo.usersName}";
        document.form1.submit();
    }
</script>

