<%@ page import="com.htdz.constant.AreaEnum" %>
<%@ page import="com.htdz.constant.ProvinceEnum" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page isELIgnored="false" %>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<jsp:include page="top.jsp" flush="true"></jsp:include>
<jsp:include page="menu.jsp" flush="true"></jsp:include>
<style>
    [class^="icon-"],
    [class*=" icon-"] {
        display: inline-block;
        width: 14px;
        height: 14px;
        *margin-right: .3em;
        line-height: 14px;
        vertical-align: text-top;
        background-image: url("<%=basePath%>img/glyphicons-halflings.png");
        background-position: 14px 14px;
        background-repeat: no-repeat;
        margin-top: 1px;
    }
    /* White icons with optional class, or on hover/active states of certain elements */
    .icon-white,
    .nav-pills > .active > a > [class^="icon-"],
    .nav-pills > .active > a > [class*=" icon-"],
    .nav-list > .active > a > [class^="icon-"],
    .nav-list > .active > a > [class*=" icon-"],
    .navbar-inverse .nav > .active > a > [class^="icon-"],
    .navbar-inverse .nav > .active > a > [class*=" icon-"],
    .dropdown-menu > li > a:hover > [class^="icon-"],
    .dropdown-menu > li > a:hover > [class*=" icon-"],
    .dropdown-menu > .active > a > [class^="icon-"],
    .dropdown-menu > .active > a > [class*=" icon-"],
    .dropdown-submenu:hover > a > [class^="icon-"],
    .dropdown-submenu:hover > a > [class*=" icon-"] {
        background-image: url("<%=basePath%>img/glyphicons-halflings-white.png");
    }
    .icon-th {
        background-position: -240px 0;
    }
</style>
<div id="mainframeContainer" class="mainframeContainer">
    <div class="breadcrumb" style="margin-bottom: 0px">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
        <li><a href="#">基础信息管理</a></li>
        <li class="active">工具栏</li>
    </div>
    <div class="self-toolbar" >
        <span class="input-append date start_datetime">
            时间：<input size="10" type="text" value="${startDate}" readonly >
            <span class="add-on"><i class="icon-th"></i></span>
        </span>
        <span class="input-append date end_datetime">
            -- <input size="10" type="text" value="${endDate}" readonly>
            <span class="add-on"><i class="icon-th"></i></span>
        </span>
        <span>
            <select id="sel" class="selectpicker" data-live-search="true" multiple data-live-search-placeholder="搜索" title="-请选择地区-" >
                <c:set var="enumValues" value="<%=AreaEnum.values()%>"/>
                <c:forEach  items="${enumValues}" var="li" >
                    <option value="${li.code}">${li.value}</option>
                </c:forEach>
            </select>
        </span>

        <span>
            <select id="prov" class="selectpicker" title="-请选择省份-" >
                <c:set var="enumValues" value="<%=ProvinceEnum.values()%>"/>
                <c:forEach  items="${enumValues}" var="li" >
                    <option value="${li.code}">${li.value}</option>
                </c:forEach>
            </select>

            <select id="city" class="selectpicker" title="-请选择地市-" >
            </select>
        </span>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="#" onclick="query()" title="查询"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
    </div>
</div>
<script type="text/javascript">
    $(".start_datetime").datetimepicker({
        language : 'zh-CN',
        format: "yyyy-mm-dd",
        minView: 2,//时间选择器最小单位，2，表示最小为日
        autoclose: true,//选中自动关闭
        todayBtn: true,
        pickerPosition: "bottom-right"
    });
    $(".end_datetime").datetimepicker({
        language : 'zh-CN',
        format: "yyyy-mm-dd",
        minView: 2,//时间选择器最小单位，2，表示最小为日
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right"
    });
    function query(){
        console.log($(".selectpicker").val());
    }
    $('#sel').selectpicker({
        'actionsBox':true,//设置全选和全不选开关
        'deselectAllText': '全不选',
        'selectAllText': '全选'
    });
    $('#sel').selectpicker('val', ['0571','0572','0573']);//设置默认选项,需设置数组
    //$('.selectpicker').selectpicker('val', '0571');//如果是单选按钮，无需数组
    $('#sel').on('hidden.bs.select', function (e) {//value 绑定Change事件
        console.log($('#sel').val());
    });
    $('#prov').on('hidden.bs.select', function (e) {//value 绑定Change事件
        console.log($('#prov').val());
        setCitySelect();
    });
    function setCitySelect(){
        $("#city").empty();
        $.post("<%=basePath%>tool/getCityListByProv",
            "provCode="+$('#prov').val(),
            function (data,status) {
                console.log("data="+JSON.stringify(data),"status="+status);
                for(var i = 0;i<data.length;i++){
                    $("#city").append("<option value='"+data[i].code+"'>"+data[i].value+"</option>");
                }
                $("#city").selectpicker('refresh');//下拉菜单重新刷新
            },
            'json'
        );
    }
</script>
</body>
</html>
