package com.htdz;

import cn.jarod.myframework.gaeaautocoding.utils.ProjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.htdz.constant.AreaEnum;
import com.htdz.entity.RespData;
import com.htdz.entity.UsersInfo;
import com.htdz.util.CommonRedisUtil;
import com.htdz.util.JsonUtil;
import com.htdz.util.PropertiesUtil;
import org.junit.Assert;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.Assert.assertEquals;


public class ControllerTest {

    //生成mapper
    @Test
    public void createBaseFrameworkTest(){
        String _url = "jdbc:mysql://localhost:3306/netease?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull";
        String _name =  "root";
        String _pwd =  "";
        ProjectUtil.createBaseFramework("com.htdz.service" , "Users", "monkeyKingService", _url, _name, _pwd);
    }

    @Test
    public void numTest(){
        short a = 1;
        int b = 1;
        System.out.println(a==(Integer)1);
    }

    @Test
    public void redisTest(){
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("localhost",6379);
        jedis.auth("admin.1231");
        System.out.println("连接成功");
        //设置 redis 字符串数据
        //jedis.set("runoobkey", "www.runoob.com");
        // 获取存储的数据并输出
        System.out.println("redis 存储的字符串为: "+ jedis.get("runoobkey"));
    }

    @Test
    public void redisTest2(){
        //CommonRedisUtil.getInstance().set("runoobkey","www.runoob.com",60);
        /*CommonRedisUtil redisUtil = new CommonRedisUtil();
        String result = redisUtil.getJedis().get("runoobkey");
        System.out.println(result);
*/
        String userInfo = "{\"usersId\":2,\"usersName\":\"transfar\"}";
        UsersInfo usersInfo = JsonUtil.json2obj(userInfo,UsersInfo.class);
        assertEquals("transfar",usersInfo.getUsersName());
    }

    @Test
    public void test(){
        System.out.println(AreaEnum.HANG_ZHOU.getCode());
        System.out.println(AreaEnum.getCodeByValue("杭州市"));

        System.out.println(PropertiesUtil.getPropertieValue("helper.message"));
        Assert.assertTrue(PropertiesUtil.getPropertieValue("helper.message").equals("ok_helper_message"));

        ConcurrentLinkedQueue conLinkedQueue = new ConcurrentLinkedQueue();
        for(int i = 0;i<100;i++){
            conLinkedQueue.add(i);
        }
        while(!conLinkedQueue.isEmpty()){
            //Integer pollStr = (Integer) conLinkedQueue.poll();
            System.out.println(conLinkedQueue.poll());
            System.out.println(conLinkedQueue.poll());
        }


    }

}
