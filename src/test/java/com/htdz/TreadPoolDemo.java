package com.htdz;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TreadPoolDemo {
    public static class MyTask implements Runnable{

        static ConcurrentLinkedQueue conLinkedQueue = new ConcurrentLinkedQueue();
        static {
            for(int i = 0;i<100;i++){
                conLinkedQueue.add(i);
            }
        }

        @Override
        public void run() {
            while(!conLinkedQueue.isEmpty()){
                System.out.println("Thread Id :"+Thread.currentThread().getId()+" "+conLinkedQueue.poll());

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args){
        MyTask task = new MyTask();
        ExecutorService  executorService = Executors.newFixedThreadPool(5);
        for(int i = 0;i<10;i++){
            executorService.submit(task);
        }

    }
}
