package com.htdz;

import com.htdz.webservice.cxf.DTSaleArea2CityDistReq;
import com.htdz.webservice.cxf.DTSaleArea2CityDistRes;
import com.htdz.webservice.cxf.SISaleArea2CityDistOut;
import com.htdz.webservice.cxf.SISaleArea2CityDistOutService;
import org.junit.Test;

import javax.xml.ws.BindingProvider;
import java.util.List;
import java.util.Map;

public class Q30100Test {

    @Test
    public void q30100(){

        //设置请求参数
        DTSaleArea2CityDistReq dtSaleArea2CityDistReq = new DTSaleArea2CityDistReq();
        dtSaleArea2CityDistReq.setBEGDATE("2017-11-02");

        //服务类
        SISaleArea2CityDistOutService service = new SISaleArea2CityDistOutService();
        SISaleArea2CityDistOut out = service.getHTTPPort();

        //设置webservice用户名密码，接口验证
        BindingProvider bp = (BindingProvider) out;
        Map<String, Object> context = bp.getRequestContext();
        context.put(BindingProvider.USERNAME_PROPERTY, "pisuper");
        context.put(BindingProvider.PASSWORD_PROPERTY, "Transfar2016");

        //调用具体方法名称
        DTSaleArea2CityDistRes dtSaleArea2CityDistRes = out.siSaleArea2CityDistOut(dtSaleArea2CityDistReq);

        List<DTSaleArea2CityDistRes.ITEMS> items  =  dtSaleArea2CityDistRes.getITEMS();

        for(DTSaleArea2CityDistRes.ITEMS item : items){
            System.out.println(item.getBZIRK()+" || "+item.getBZTXT()+" || "+item.getERDATE()+" || "+item.getPID());
        }
    }
}
